package newrelic

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

type terraformWriter struct {
	buffer *strings.Builder
	syntaxIndent
}

func newTerraformWriter() *terraformWriter {
	return &terraformWriter{&strings.Builder{}, syntaxIndent{}}
}

func (tfWriter *terraformWriter) String() string {
	return tfWriter.buffer.String()
}

func (tfWriter *terraformWriter) indent() terraformWriter {
	return terraformWriter{tfWriter.buffer, tfWriter.syntaxIndent.increment()}
}

func (tfWriter *terraformWriter) writeIndent() {
	if tfWriter.level > 0 {
		fmt.Fprint(tfWriter.buffer, tfWriter.prefix)
	}
}

func (tfWriter *terraformWriter) writeResource(resType string, resName string, callback func(tfWriter terraformWriter)) {
	header := fmt.Sprintf(`resource "%s" "%v"`, resType, resName)
	tfWriter.writeBlock(header, callback)
}

func (tfWriter *terraformWriter) writeBlock(blockHeader string, callback func(tfWriter terraformWriter)) {
	tfWriter.writeLine(blockHeader + " {")
	callback(tfWriter.indent())
	tfWriter.writeLine("}")
}

func (tfWriter *terraformWriter) writeLn() {
	fmt.Fprintln(tfWriter.buffer)
}

func (tfWriter *terraformWriter) writeLine(args ...any) {
	tfWriter.writeIndent()
	fmt.Fprint(tfWriter.buffer, args...)
	tfWriter.writeLn()
}

func (tfWriter *terraformWriter) writeProperty(property string, valueAny any) {
	value := toValue(reflect.ValueOf(valueAny), tfWriter.syntaxIndent)
	tfWriter.writeLine(property + " = " + value)
}

func (tfWriter *terraformWriter) writePropertyNonZero(property string, valueAny any) bool {
	if isZeroValue(valueAny) {
		return false
	}

	tfWriter.writeProperty(property, valueAny)
	return true
}

func toValue(original reflect.Value, indent syntaxIndent) string {
	switch original.Kind() {
	case reflect.Struct:
		switch value := original.Interface().(type) {
		case heredocLiteral:
			return value.toValue(indent)
		case JsonLinkedEntityGuid:
			return `"` + escapeQuotedString(value.Guid) + `"`
		}

	case reflect.Array, reflect.Slice:
		len := original.Len()
		if len == 0 {
			return "[]"
		}
		out := "[\n"
		for i := 0; i < len; i++ {
			extraIndent := indent.increment()
			out += extraIndent.prefix + toValue(original.Index(i), extraIndent) + ",\n"
		}
		out += indent.prefix + "]"
		return out

	case reflect.String:
		return `"` + escapeQuotedString(original.String()) + `"`

	case reflect.Int:
		return strconv.Itoa(int(original.Int()))

	case reflect.Float64:
		return fmt.Sprintf("%f", original.Float())

	case reflect.Bool:
		return strconv.FormatBool(original.Bool())
	}

	panic(fmt.Sprintf("Can't handle type %v", original.Kind()))
}

func isZeroValue(valueAny any) bool {
	if valueAny == nil {
		return true
	}
	switch value := valueAny.(type) {
	case string:
		return value == ""
	case heredocLiteral:
		return value.value == ""
	case int:
		return value == 0
	case bool:
		return !value
	default:
		valueType := reflect.ValueOf(valueAny)
		switch valueType.Kind() {
		case reflect.Array, reflect.Slice:
			return valueType.Len() == 0
		}
	}
	return false
}

//
// Syntax Indent (leading spaces indentation)
//

type syntaxIndent struct {
	level  int
	prefix string
}

func newSyntaxIndent(level int) syntaxIndent {
	prefix := strings.Repeat("  ", level)
	return syntaxIndent{level, prefix}
}

func (indent syntaxIndent) increment() syntaxIndent {
	return newSyntaxIndent(indent.level + 1)
}

//
// Heredoc
//

type heredocLiteral struct {
	delimiter string
	value     string
}

// Wraps the given string in a heredoc if the string has multiple lines (\n).
// If the string is single lined then the string is returned verbatim.
func heredoc(delimiter string, value string) any {
	if strings.Contains(value, "\n") {
		return heredocLiteral{delimiter, value}
	}
	return value
}

func (heredoc heredocLiteral) toValue(indent syntaxIndent) string {
	value := heredoc.value
	value = FixIndentation(value, indent.increment().prefix)
	value = strings.TrimRight(value, " \n")

	builder := strings.Builder{}

	builder.WriteString("<<-")
	builder.WriteString(heredoc.delimiter)
	builder.WriteString("\n")

	builder.WriteString(value)
	builder.WriteString("\n")

	builder.WriteString(indent.prefix)
	builder.WriteString(heredoc.delimiter)

	return builder.String()
}
