package newrelic

import (
	"reflect"
	"testing"
)

func TestAccountIdCorrector(t *testing.T) {
	tests := []struct {
		accountId int
		expected  int
	}{
		{0, 0},
		{123, 0},
		{234, 234},
	}

	corrector := AccountIdCorrector(123)

	for _, test := range tests {
		got := corrector(test.accountId)
		if got != test.expected {
			t.Errorf("AccountIdCorrector() test failed, got: %v, expected: %v.", got, test.expected)
		}
	}
}

func TestCleanDashboardAccountId(t *testing.T) {
	corrector := AccountIdCorrector(123)
	createWidget := func(accountId int) JsonWidget {
		return JsonWidget{
			Title: "Widget",
			RawConfiguration: JsonRawConfiguration{
				NrqlQueries: []JsonNrqlQuery{
					{
						AccountId: accountId,
						Query:     "SELECT foo",
					},
				},
			},
		}
	}
	dashboard := JsonDashboard{
		Name: "Test",
		Pages: []JsonPage{
			{
				Name: "1st page",
				Widgets: []JsonWidget{
					createWidget(0),
					createWidget(123),
					{
						Title: "Widget 3",
						RawConfiguration: JsonRawConfiguration{
							NrqlQueries: []JsonNrqlQuery{
								{
									Query: "SELECT foo3",
								},
							},
						},
					},
				},
			},

			{
				Name: "2nd page",
				Widgets: []JsonWidget{
					createWidget(0),
					createWidget(123),
					createWidget(234),
				},
			},
		},
	}

	CleanDashboardAccountId(&dashboard, corrector)

	got := []int{}
	for pageIdx := range dashboard.Pages {
		page := &dashboard.Pages[pageIdx]
		for widgetIdx := range page.Widgets {
			widget := &page.Widgets[widgetIdx]
			for nrqlQueryIdx := range widget.RawConfiguration.NrqlQueries {
				nrqlQuery := &widget.RawConfiguration.NrqlQueries[nrqlQueryIdx]
				got = append(got, nrqlQuery.AccountId)
			}
		}
	}

	expected := []int{0, 0, 0, 0, 0, 234}

	if !reflect.DeepEqual(got, expected) {
		t.Errorf("CleanDashboardAccountId() test failed, got: %v, expected: %v.", got, expected)
	}
}

func TestWidgetName(t *testing.T) {
	tests := []struct {
		name     string
		expected string
	}{
		{"viz.area", "widget_area"},
		{"viz.bar", "widget_bar"},
		{"viz.billboard", "widget_billboard"},
		{"viz.bullet", "widget_bullet"},
		{"viz.funnel", "widget_funnel"},
		{"viz.heatmap", "widget_heatmap"},
		{"viz.histogram", "widget_histogram"},
		{"viz.json", "widget_json"},
		{"viz.line", "widget_line"},
		{"viz.markdown", "widget_markdown"},
		{"viz.pie", "widget_pie"},
		{"viz.stacked_bar", "widget_stacked_bar"},
		{"viz.table", "widget_table"},
	}

	for _, test := range tests {
		got := widgetName(test.name)
		if got != test.expected {
			t.Errorf("widgetName() test failed, got: %v, expected: %v.", got, test.expected)
		}
	}
}

func TestFixIndentation(t *testing.T) {
	tests := []struct {
		value    string
		indent   string
		expected string
	}{
		{"", "", ""},
		{"", "  ", "  "},
		{"hello world 1", "", "hello world 1"},
		{"hello world 2", "  ", "  hello world 2"},
		{"  hello\n  world\n  3", "|-->", "|-->hello\n|-->world\n|-->3"},
		{"  hello\n    world\n  4", "|-->", "|-->hello\n|-->  world\n|-->4"},
	}

	for _, test := range tests {
		got := FixIndentation(test.value, test.indent)
		if got != test.expected {
			t.Errorf("FixIndentation() test failed, got: %v, expected: %v.", got, test.expected)
		}
	}
}
