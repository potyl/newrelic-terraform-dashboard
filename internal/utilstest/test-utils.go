package utilstest

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

var AccountIdCorrector = newrelic.AccountIdCorrector(1234567)

func Compare(t *testing.T, name string, got any, expected any) {
	if !reflect.DeepEqual(got, expected) {
		t.Errorf("%s test failed\ngot:      %#v\nexpected: %#v", name, got, expected)
	}
}

func TerraformContent(content string) string {
	content = strings.TrimLeft(content, "\r\n")
	content = newrelic.FixIndentation(content, "")
	content = strings.ReplaceAll(content, "\t", "  ")
	content = strings.TrimRight(content, " \r\n")
	content += "\n"

	return content
}
