package commands

import (
	_ "embed"
	"testing"

	"gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
	"gitlab.com/potyl/newrelic-dashboard/internal/utilstest"
)

//go:embed fixtures/viz-bullet.json
var jsonDashboardBullet []byte

func TestParseDashboardBullet(t *testing.T) {
	got := parseJsonDashboardBytes(jsonDashboardBullet, utilstest.AccountIdCorrector)

	expected := newrelic.JsonDashboard{
		Name:        "Test",
		Permissions: "PUBLIC_READ_WRITE",
		Pages: []newrelic.JsonPage{
			{
				Name: "My Bullets",
				Widgets: []newrelic.JsonWidget{
					{
						Title: "Transactions Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    2,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.bullet",
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									Query: "SELECT uniqueCount(session) FROM PageView SINCE 1 day ago",
								},
							},
							Limit: 10.5,
						},
					},

					{
						Title: "Errors Overview",
						Layout: newrelic.JsonLayout{
							Column: 6,
							Row:    5,
							Width:  7,
							Height: 3,
						},
						Visualization: newrelic.JsonVisualization{
							Id: "viz.bullet",
						},
						RawConfiguration: newrelic.JsonRawConfiguration{
							NrqlQueries: []newrelic.JsonNrqlQuery{
								{
									AccountId: 2345678,
									Query:     "SELECT count(*) FROM Errors",
								},
							},
							Limit: 10,
						},
					},
				},
			},
		},
	}

	utilstest.Compare(t, "parseDashboard(bullet)", got, expected)
}

func TestTerraformDashboardBullet(t *testing.T) {
	dashboard := parseJsonDashboardBytes(jsonDashboardBullet, utilstest.AccountIdCorrector)
	got := newrelic.ToTerraform(dashboard, "bullet")

	expected := utilstest.TerraformContent(`
		resource "newrelic_one_dashboard" "bullet" {
			name        = "Test"
			permissions = "public_read_write"

			page {
				name = "My Bullets"

				widget_bullet {
					title = "Transactions Overview"

					row    = 2
					column = 6
					width  = 7
					height = 3

					limit = 10.500000

					nrql_query {
						query = "SELECT uniqueCount(session) FROM PageView SINCE 1 day ago"
					}
				}

				widget_bullet {
					title = "Errors Overview"

					row    = 5
					column = 6
					width  = 7
					height = 3

					limit = 10.000000

					nrql_query {
						query      = "SELECT count(*) FROM Errors"
						account_id = 2345678
					}
				}
			}
		}
	`)

	utilstest.Compare(t, "terraform(bullet)", got, expected)
}
