package commands

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	nrDashboard "gitlab.com/potyl/newrelic-dashboard/internal/newrelic"
)

func init() {
	rootCmd.AddCommand(listCommand())
}

type listFlagsType struct {
	accountId       int
	jsonExport      bool
	search          string
	showParentsOnly bool
}

func listCommand() *cobra.Command {
	var flags = listFlagsType{}

	var cmd = &cobra.Command{
		Use:   "list",
		Short: "Prints the dashboards",
		Long: longDoc(`
			Prints the newrelic dashboards available.
			By default all dashboards are printed.
			The dashboards can be filtered out by name with --search and by account id with --account-id.
		`),
		Run: func(cmd *cobra.Command, args []string) {
			listCmdRun(cmd, args, flags)
		},
	}

	cmd.Flags().StringVarP(&flags.search, "search", "s", flags.search, "the search words to use for finding dashboards")
	cmd.Flags().IntVarP(&flags.accountId, "account-id", "a", flags.accountId, "the account id")
	cmd.Flags().BoolVarP(&flags.jsonExport, "json", "j", flags.jsonExport, "download a dashboard in json format")
	cmd.Flags().BoolVarP(&flags.showParentsOnly, "parents", "p", flags.showParentsOnly, "display parent dashboards only ")

	return cmd
}

func listCmdRun(cmd *cobra.Command, args []string, flags listFlagsType) {
	client := nrDashboard.NewRelicClient()

	response, err := nrDashboard.ListDashboards(client, flags.search)
	if err != nil {
		log.Fatal("Failed to fetch the dashboards: ", err)
	}

	// Filter out the dashboard by account id
	if flags.accountId != 0 {
		originalDashboards := response.Actor.EntitySearch.Results.Dashboards
		dashboards := originalDashboards[:0]
		for _, dashboard := range originalDashboards {
			if dashboard.AccountId == flags.accountId {
				dashboards = append(dashboards, dashboard)
			}
		}
		response.Actor.EntitySearch.Results.Dashboards = dashboards
	}

	if flags.jsonExport {
		output := nrDashboard.ToJson(response)
		fmt.Print(output)
		return
	}

	for _, dashboard := range response.Actor.EntitySearch.Results.Dashboards {
		if !flags.showParentsOnly || dashboard.DashboardParentGuid == "" {
			fmt.Printf("%v (%v) guid: %v\n", dashboard.Name, dashboard.AccountId, dashboard.Guid)
		}
	}
}
