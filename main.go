package main

import (
	"gitlab.com/potyl/newrelic-dashboard/commands"
)

// Version of the application
var version = "devel"

func main() {
	commands.Execute(version)
}
